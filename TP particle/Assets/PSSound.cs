﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PSSound : MonoBehaviour {
    public AudioClip launchSound, explosionSound;
    public AudioSource audioSourceExplosion, audioSourceLaunch;
    ParticleSystem m_particleSystem;
    ParticleSystem.Particle[] m_particles;
    float[] m_times;

    void Awake()
    {
      
        m_particleSystem = GetComponent<ParticleSystem>();
        m_times = new float[m_particleSystem.main.maxParticles];
        m_particles = new ParticleSystem.Particle[m_particleSystem.main.maxParticles];
    }

    void LateUpdate()
    {
        m_particleSystem.GetParticles(m_particles);
        for (int i = 0; i < m_particles.Length; ++i)
        {
            if (m_times[i] < m_particles[i].remainingLifetime && m_particles[i].remainingLifetime > 0)
            {
                // Birth
                StartCoroutine(Play(m_particles[i].remainingLifetime));
            }

            m_times[i] = m_particles[i].remainingLifetime;
        }
    }

    IEnumerator Play(float lifetime)
    {
        /************* Début de la procedure executée à la naissance de la particule ****************/
        // Ajouter une audio source (Indice : Utilisez AddComponent<AudioSource>() -> allez voir la doc pour savoir comment l'utiliser)
        audioSourceLaunch = gameObject.AddComponent<AudioSource>();
        // Assigner le son launchSound à l'audio source créée juste au dessus.
        audioSourceLaunch.clip = launchSound;
        // Jouer le son
        audioSourceLaunch.Play();
        // Prevoir la destruction de l'audio source à la fin du son. (Indice : Utilisez la fonction Destroy(...,...), 1er parametre -> ce qu'il faut détruire, 2eme parametre -> dans combien de temps il faut détruire)
        Destroy(audioSourceLaunch, launchSound.length);
        /************* Fin de la procedure executée à la naissance de la particule ****************/
        yield return new WaitForSeconds(lifetime);
        /************* Début de la procedure executée à la mort de la particule ****************/
        // Ajouter une autre audio source 
        audioSourceExplosion = gameObject.AddComponent<AudioSource>();
        // Assigner le son explosionSound à l'audio source créée juste au dessus
        audioSourceExplosion.clip = explosionSound;
        // Jouer le son
        audioSourceExplosion.Play();
        // Prevoir la destruction de l'audio source à la fin du son (Indice : Utilisez la fonction Destroy(...,...), 1er parametre -> ce qu'il faut détruire, 2eme parametre -> dans combien de temps il faut détruire)
        Destroy(audioSourceExplosion, explosionSound.length);
        /************* Fin de la procedure executée à la mort de la particule ****************/

    }
}
