﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maillet : MonoBehaviour {

    public GameManager gameManager;
    public GameObject Boom;
    //public Transform taupeTarget;
    public Transform center;

    public void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }

    public void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Taupe"))
        {
            //Taupe taupe = other.gameObject.GetComponent<Taupe>().
            //

            Vector3 mailletToTaupe = other.transform.position  - center.position;// other.transform.up*2;
           // float cosAngle = Vector3.Dot(other.transform.right, mailletToTaupe.normalized);
           // //Debug.Log(cosAngle);
           // cosAngle = Vector3.Dot(other.transform.forward, mailletToTaupe.normalized);
           //// Debug.Log(cosAngle);
            float cosAngle = Vector3.Dot(other.transform.up, mailletToTaupe.normalized);
            Debug.Log(cosAngle);
            //            Gizmos.DrawLine(other.transform.position, transform.position);

            if (cosAngle < -0.7f)
            {
                Destroy(other.gameObject);
                if (other.gameObject.GetComponent<Taupe>().type == 1)
                {
                    gameManager.point -= 10;
                }
                else
                {
                    gameManager.point += 10;
                }
                Instantiate(Boom, other.transform.position, Quaternion.identity);
            }
        }
    }

    //public void OnDrawGizmos()
    //{
    //    Gizmos.DrawLine(taupeTarget.position , center.position);
    //    Gizmos.DrawLine(taupeTarget.position , taupeTarget.position+Vector3.up);
    //}
}
