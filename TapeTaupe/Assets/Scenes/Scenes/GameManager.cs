﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject taupe, taupeMec;
    public List<Spawner> spawn;
    public int point;
    public int ratioTaupe;
    public TextMesh pointsDisplay;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreateTaupe());

    }

    // Update is called once per frame
    void Update()
    {
        pointsDisplay.text = "Points  : " + point;
    }

    IEnumerator CreateTaupe()
    {
        while (true)
        {

            int ranSpawn = Random.Range(0, spawn.Count);
            while (spawn[ranSpawn].taupeIsIn) {
                ranSpawn = Random.Range(0, spawn.Count);
            }

            int ranType = Random.Range(0, ratioTaupe);
            Instantiate(ranType == 1 ? taupeMec:taupe, spawn[ranSpawn].transform.position, Quaternion.identity);
            
            float ranf = Random.Range(0.25f,1f);
            yield return new WaitForSeconds(ranf);
        }   
    }
}
